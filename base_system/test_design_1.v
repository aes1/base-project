`timescale 1ns / 1ps

module test_design_1 ();

wire [3:0] leds_4bits_tri_i;
wire [3:0] leds_4bits_tri_o;
wire [3:0] leds_4bits_tri_t;
reg  reset_rtl;
wire [3:0] sws_4bits_tri_i;
reg  sys_clock;
design_1 DUT (
  .leds_4bits_tri_i (leds_4bits_tri_i),
  .leds_4bits_tri_o (leds_4bits_tri_o),
  .leds_4bits_tri_t (leds_4bits_tri_t),
  .reset_rtl (reset_rtl),
  .sws_4bits_tri_i (sws_4bits_tri_i),
  .sys_clock (sys_clock)
);

always
begin
  sys_clock = 0;
  #4.0;
  sys_clock = 1;
  #4.0;
end
initial
begin
  reset_rtl = 1;
  #100;
  reset_rtl = 0;
end

endmodule

